import { permissionCheckWithProjectId } from './permission.service';
import { PolicyT } from 'bovaljaren-models';
import { permissionCheck } from '.';

const mockSuperUser: { [key: string]: PolicyT } = {
    'all': {
        policyTemplateName: 'SuperUser',
        policy: [
            {
                action: ['read', 'create', 'update', 'delete'],
                resource: ['*']
            }
        ]
    }
};


describe('test', () => {
    it('should always allow (superuser)', () => {
        expect(permissionCheckWithProjectId(1, mockSuperUser, 'read', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(1, mockSuperUser, 'create', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(1, mockSuperUser, 'delete', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(1, mockSuperUser, 'update', 'project:project')).toBe(true);
        expect(permissionCheck(mockSuperUser, 'read', 'project:project')).toBe(true);
        expect(permissionCheck(mockSuperUser, 'create', 'project:project')).toBe(true);
        expect(permissionCheck(mockSuperUser, 'delete', 'project:project')).toBe(true);
        expect(permissionCheck(mockSuperUser, 'update', 'project:project')).toBe(true);
    });
});

const mockReader: { [key: string]: PolicyT } = {
    'all': {
        policyTemplateName: 'reader',
        policy: [
            {
                action: ['read'],
                resource: ['*']
            }
        ]
    }
};

describe('test', () => {
    it('should allow to read', () => {
        expect(permissionCheckWithProjectId(1, mockReader, 'read', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(1, mockReader, 'create', 'project:project')).toBe(false);
        expect(permissionCheckWithProjectId(1, mockReader, 'delete', 'project:project')).toBe(false);
        expect(permissionCheckWithProjectId(1, mockReader, 'update', 'project:project')).toBe(false);
        expect(permissionCheck(mockReader, 'read', 'project:project')).toBe(true);
        expect(permissionCheck(mockReader, 'create', 'project:project')).toBe(false);
        expect(permissionCheck(mockReader, 'delete', 'project:project')).toBe(false);
        expect(permissionCheck(mockReader, 'update', 'project:project')).toBe(false);
    });
});

const proj1mockReader: { [key: string]: PolicyT } = {
    '1': {
        policyTemplateName: 'reader',
        policy: [
            {
                action: ['read'],
                resource: ['*']
            }
        ]
    }
};

describe('test', () => {
    it('should allow to only read one project', () => {
        expect(permissionCheckWithProjectId(1, proj1mockReader, 'read', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(2, proj1mockReader, 'read', 'project:project')).toBe(false);
        expect(permissionCheckWithProjectId(4, proj1mockReader, 'read', 'project:project')).toBe(false);
        expect(permissionCheck(proj1mockReader, 'read', 'project:project')).toBe(true);
    });
});

const proj12mockReader: { [key: string]: PolicyT } = {
    '1': {
        policyTemplateName: 'reader',
        policy: [
            {
                action: ['read'],
                resource: ['*']
            }
        ]
    },
    '2': {
        policyTemplateName: 'reader',
        policy: [
            {
                action: ['read'],
                resource: ['*']
            }
        ]
    }
};

describe('test', () => {
    it('allow multiple allowed projects', () => {
        expect(permissionCheckWithProjectId(1, proj12mockReader, 'read', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(2, proj12mockReader, 'read', 'project:project')).toBe(true);
        expect(permissionCheckWithProjectId(4, proj12mockReader, 'read', 'project:project')).toBe(false);
    });
});