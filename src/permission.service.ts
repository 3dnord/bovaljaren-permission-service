import { ActionT, PolicyT } from 'bovaljaren-models';


/**
 * En stjärna (*) indikerar endast en positon i resource.
 */
type SubPolicy = {action: ActionT[], resource: string[]};

const permissionCheckWithProjectId = (projectId: number, policies: { [key: string]: PolicyT | null }, action: ActionT, resource: string): boolean => {
    let result = false;

    if (!policies || Object.keys(policies).length === 0) return false;

    if (policies?.all && filterActions(policies.all, action).length > 0) {
        result = checkResources(policies.all.policy, resource);
    }
    else{
        Object.keys(policies).forEach(key => {

            if (parseInt(key) === projectId) {
                const subpolicies = filterActions(policies[key], action);
                if (subpolicies.length > 0) {
                    result = checkResources(subpolicies, resource);
                }
            }
        });
    }
    return result;

};

const permissionCheck = (policies: { [key: string]: PolicyT | null }, action: ActionT, resource: string): boolean => {
    let result = false;

    if (!policies || Object.keys(policies).length === 0) return false;

    if (policies.all && filterActions(policies.all, action).length > 0) {
        result = checkResources(policies.all.policy, resource);
    }
    else{
        Object.keys(policies).forEach(key => {

            const subpolicies =  filterActions(policies[key], action);
    
            if (subpolicies.length > 0) {
                result = checkResources(subpolicies, resource);
            }
        });
    }
    return result;

};

const filterActions = (bovaljarenPolicy: PolicyT | null, action: ActionT): SubPolicy[] => {
    if(!bovaljarenPolicy?.policy) return [];

    return bovaljarenPolicy.policy
        .filter(p => p.action.indexOf(action) !== -1);
};


const checkResources = (subpolicies: SubPolicy[], resource: string): boolean => {
    const result = !!subpolicies.some(policy => {
        return policy.resource.find(pR => checkResource(pR, resource));
    });

    return result;
};


const checkResource = (policyResource: string, resource: string) => {
    {
        if (policyResource.indexOf('*') !== -1) {
            return stringMatch(policyResource, resource);
        }
        else {
            return policyResource === resource;
        }
    }
};


const stringMatch = (policyString: string, requestString:string): boolean => {
    const pSplit = policyString.split(':');
    const rSplit = requestString.split(':');

    let result = true;

    pSplit.forEach((s, i) => {
        if (!result) return;

        if (s === rSplit[i]) {
            // all good
        }
        else if (s === '*') {
            // all good
        }
        else {
            // all not good
            result = false;
        }
    });

    return result;
};

export { permissionCheckWithProjectId, permissionCheck };