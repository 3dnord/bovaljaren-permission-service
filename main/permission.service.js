"use strict";
exports.__esModule = true;
exports.permissionCheck = exports.permissionCheckWithProjectId = void 0;
var permissionCheckWithProjectId = function (projectId, policies, action, resource) {
    var result = false;
    if (!policies || Object.keys(policies).length === 0)
        return false;
    if ((policies === null || policies === void 0 ? void 0 : policies.all) && filterActions(policies.all, action).length > 0) {
        result = checkResources(policies.all.policy, resource);
    }
    else {
        Object.keys(policies).forEach(function (key) {
            if (parseInt(key) === projectId) {
                var subpolicies = filterActions(policies[key], action);
                if (subpolicies.length > 0) {
                    result = checkResources(subpolicies, resource);
                }
            }
        });
    }
    return result;
};
exports.permissionCheckWithProjectId = permissionCheckWithProjectId;
var permissionCheck = function (policies, action, resource) {
    var result = false;
    if (!policies || Object.keys(policies).length === 0)
        return false;
    if (policies.all && filterActions(policies.all, action).length > 0) {
        result = checkResources(policies.all.policy, resource);
    }
    else {
        Object.keys(policies).forEach(function (key) {
            var subpolicies = filterActions(policies[key], action);
            if (subpolicies.length > 0) {
                result = checkResources(subpolicies, resource);
            }
        });
    }
    return result;
};
exports.permissionCheck = permissionCheck;
var filterActions = function (bovaljarenPolicy, action) {
    if (!(bovaljarenPolicy === null || bovaljarenPolicy === void 0 ? void 0 : bovaljarenPolicy.policy))
        return [];
    return bovaljarenPolicy.policy
        .filter(function (p) { return p.action.indexOf(action) !== -1; });
};
var checkResources = function (subpolicies, resource) {
    var result = !!subpolicies.some(function (policy) {
        return policy.resource.find(function (pR) { return checkResource(pR, resource); });
    });
    return result;
};
var checkResource = function (policyResource, resource) {
    {
        if (policyResource.indexOf('*') !== -1) {
            return stringMatch(policyResource, resource);
        }
        else {
            return policyResource === resource;
        }
    }
};
var stringMatch = function (policyString, requestString) {
    var pSplit = policyString.split(':');
    var rSplit = requestString.split(':');
    var result = true;
    pSplit.forEach(function (s, i) {
        if (!result)
            return;
        if (s === rSplit[i]) {
            // all good
        }
        else if (s === '*') {
            // all good
        }
        else {
            // all not good
            result = false;
        }
    });
    return result;
};
