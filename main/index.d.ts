import { ActionT, PolicyT } from 'bovaljaren-models';

declare module 'bovaljaren-permission-service' {
    export function permissionCheckWithProjectId(projectId: number, policies: { [key: string]: PolicyT | null }, action: ActionT, resource: string): boolean;
    export function permissionCheck(policies: { [key: string]: PolicyT | null }, action: ActionT, resource: string): boolean;
}