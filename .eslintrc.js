module.exports = {
    parser: '@typescript-eslint/parser', // Specifies the ESLint parser
    parserOptions: {
        ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
        sourceType: 'module', // Allows for the use of imports
        ecmaFeatures: {
            jsx: true // Allows for the parsing of JSX
        }
    },
    settings: {
        react: {
            version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
        }
    },
    extends: [
        'plugin:@typescript-eslint/recommended' // Uses the recommended rules from @typescript-eslint/eslint-plugin
    ],
    rules: {
        'react/prop-types': 0,
        'no-var': 2,
        'indent': ['error', 4],
        'quotes': [2, 'single'],
        'no-trailing-spaces': 2,
        'eqeqeq': 2,
        'no-useless-concat': 2,
        'no-multi-spaces': 2,
        'no-useless-return': 2,
        'block-spacing': 2,
        'space-before-blocks': 2,
        'comma-spacing': 2,
        'keyword-spacing':2,
        'object-curly-spacing':[2, 'always'],
        'array-bracket-spacing':2,
        'computed-property-spacing':2,
        'space-in-parens': ['error', 'never'],
        'semi':[2, 'always'],
        'space-infix-ops': 'error',
        'newline-before-return':2,
        'padding-line-between-statements': [
            'warn',
            { 'blankLine': 'always', 'prev': '*', 'next': 'if' }
        ],
        'brace-style':[2, 'stroustrup'],
        'prefer-const':2,
        'arrow-spacing':[2, { 'before': true, 'after': true }],
        'comma-dangle': [2, 'never'],
        'dot-location': ['error', 'property'],
        'operator-linebreak': [2, 'after'],
        'newline-per-chained-call': ['error', { 'ignoreChainWithDepth': 2 }],
        'no-lonely-if':1,
        'no-mixed-operators':1,
        'prefer-template':2,
        'no-extra-semi':[0, 'always']

    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    // e.g. '@typescript-eslint/explicit-function-return-type': 'off',
    }
};
